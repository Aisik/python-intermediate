from datetime import datetime


def function_tracking(function):
    def wrapper():
        print("function started")
        function()
        print("function ended")

    return wrapper


def first_decorator(function):
    def wrapper():
        print("first decorator started")
        function()
        print("first decorator ended")

    return wrapper


@first_decorator
@function_tracking
# --> print_today = first_decorator(function_tracking(print_today))
def print_today():
    print(datetime.now())


print_today()
