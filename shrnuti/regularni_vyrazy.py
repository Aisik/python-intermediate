import re


# napiste program, ktery zmeni datum z formatu YYY-MM-DD na DD-MM-YYYY format
def zmen_format_datumu(dt):
    return re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', r'\3-\2-\1', dt)


datum = "2026-01-02"
print("Originalni datum ve formatu YYY-MM-DD Format: ", datum)
print("Nove datum ve formatu DD-MM-YYYY Format: ", zmen_format_datumu(datum))


# alternativa nevyuzivajici regularni vyrazy
from datetime import datetime


stare_datum = datetime.strptime(datum, "%Y-%m-%d")
print(stare_datum)
nove_datum = stare_datum.strftime("%d-%m-%Y")
print(nove_datum)
