# TODO implementovat retry dekorator
# pomoci funkce a pote klidne i pomoci tridy

# kapitola Decorators and separation of concerns

# mro - https://www.educative.io/answers/what-is-mro-in-python
# zopaknout rozdil mezi match a search
# GIL ze neblokuje I/O operace
# generator comprehension

import random


def opakuj_pri_chybe(dekorovana_funkce):
    def wrapped(*args, **kwargs):
        posledni_vyjimka = None
        POCET_OPAKOVANI = 3
        for _ in range(POCET_OPAKOVANI):
            try:
                return dekorovana_funkce(*args, **kwargs)
            except Exception as e:
                print(f"Znovu poustim operaci {dekorovana_funkce.__name__}, pokud nebyl prekonan limit!")
                posledni_vyjimka = e
        raise posledni_vyjimka

    return wrapped


POCET_OPAKOVANI = 3


def opakuj_pri_chybe_lepe(pocet_opakovani=POCET_OPAKOVANI, povolene_vyjimky=None):
    povolene_vyjimky = povolene_vyjimky or Exception

    def opakuj(dekorovana_funkce):
        def wrapped(*args, **kwargs):
            posledni_vyjimka = None
            for _ in range(pocet_opakovani):
                try:
                    return dekorovana_funkce(*args, **kwargs)
                except povolene_vyjimky as e:
                    print(f"Zapis selhal poustim operaci {dekorovana_funkce.__name__}!")
                    posledni_vyjimka = e
            raise posledni_vyjimka

        return wrapped

    return opakuj


class ZapisovacDoSouboru:
    def __init__(self):
        self.pocet_pokusu_o_zapis = 0

    @opakuj_pri_chybe_lepe(pocet_opakovani=20, povolene_vyjimky=(Exception, ValueError))
    def zapis_do_souboru(self, text):
        with open("zkouska_opakovani.log", 'a') as soubor_zkouska:
            self.pocet_pokusu_o_zapis += 1

            nahodne_cislo = random.randint(0, 100)
            if nahodne_cislo > 20:
                if nahodne_cislo > 40:
                    raise Exception("Nepodarilo se ani opakovane zapsat do souboru!")
                else:
                    raise ValueError("Nepodarila se zapsat dana hodnota!")
            # timto jde jednoduse ukazat!
            # for i in range(5):
            #     raise Exception
            soubor_zkouska.write(f"{text} - Pokus cislo: {self.pocet_pokusu_o_zapis}\n")
            self.pocet_pokusu_o_zapis = 0


zapisovac = ZapisovacDoSouboru()
for i in range(5):
    zapisovac.zapis_do_souboru(f"Zapis cislo {i + 1}.")




# PROC NEPOUZIVAT MUTABLE as default:
def pridej_prvek_do_listu(prvek, vstupni_list=None):
    vstupni_list.append(prvek)
    return vstupni_list


print(pridej_prvek_do_listu("ahoj", []))
print(pridej_prvek_do_listu("opakovani", ["dnes", "delame"]))
print(pridej_prvek_do_listu("svete", []))


# # https://florimond.dev/en/posts/2018/08/python-mutable-defaults-are-the-source-of-all-evil/
# # vzdy je potreba dat None u listu!!!!
# def compute_patterns(inputs=[]):
#     inputs.append("some stuff")
#     patterns = ["a list based on"] + inputs
#     return patterns
#
# # >>> compute_patterns()
# # ['a list based on', 'some stuff']  # Expected
# # >>> compute_patterns()
# # ['a list based on', 'some stuff', 'some stuff']  # Woops!
