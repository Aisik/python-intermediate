import re

# https://www.washingtonpost.com/us-policy/2022/08/07/republicans-family-benefits-roe-dobbs/
# https://www.washingtonpost.com/politics/2022/08/05/us-summons-china-ambassador/
# https://www.washingtonpost.com/health/2022/08/08/vaping-marijuana-link/
# https://www.washingtonpost.com/business/interactive/2022/home-building-cost/


def extrahuj_url(url):
    # nejdrive to pouzij bez zacatecniho a konciho lomitka
    return re.search(r'/(\d{4})(/\d{2})?(/\d{2})?/', url)


seznam_url = [
    "https://www.washingtonpost.com/us-policy/2022/08/07/republicans-family-benefits-roe-dobbs/",
    "https://www.washingtonpost.com/politics/2022/08/05/us-summons-china-ambassador/",
    "https://www.washingtonpost.com/health/2022/08/08/vaping-marijuana-link/",
    "https://www.washingtonpost.com/business/interactive/2022/home-building-cost/",
    "https://www.washingtonpost.com/business/interactive/2022/08/home-building-cost/"
]

for url in seznam_url:
    datum_match = extrahuj_url(url)

    datum = []
    for skupina in datum_match.groups():
        try:
            datum.append(skupina.replace('/', ''))
        except AttributeError:
            datum.append(None)

    rok, mesic, den = datum
    print(rok, mesic, den)
    # ukazat groups
    print(datum_match[0][1:-1])
