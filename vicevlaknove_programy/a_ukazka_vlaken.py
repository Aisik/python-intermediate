import logging
import threading
import time


def funkce_pro_vlakno(nazev_vlakna, retezec):
    # funkce, kterou predavame vlaknu jako jeho praci
    logging.info(f"Vlakno {nazev_vlakna}: start!")
    for znak in retezec:
        time.sleep(nazev_vlakna)
        logging.info(f"Vlakno {nazev_vlakna}: {znak}")
    logging.info(f"Vlakno {nazev_vlakna}: konec!")


# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes
LOGGING_DATETIME_FORMAT = "%H:%M:%S"
# https://docs.python.org/3/library/logging.html#logrecord-attributes-1
LOGGING_FORMAT = "%(asctime)s.%(msecs)03d: %(message)s"

# https://docs.python.org/3/library/logging.html#logging.basicConfig
# pro zapis do souboru lze pridat filename=cesta_k_souboru
# lze pridat filemode='a' (vychozi parametr)
logging.basicConfig(format=LOGGING_FORMAT, level=logging.INFO,
                    datefmt=LOGGING_DATETIME_FORMAT)
logger = logging.getLogger()

if __name__ == "__main__":
    logging.info("Hlavni vlakno: Pred vytvorenim vlakna!")
    prvni_vlakno = threading.Thread(target=funkce_pro_vlakno, args=(1, "Python"))
    druhe_vlakno = threading.Thread(target=funkce_pro_vlakno, args=(2, "Vlakno"))
    logging.info("Hlavni vlakno: Pred spustenim vlaken")
    prvni_vlakno.start()
    druhe_vlakno.start()
    # x.join()
    logging.info("Hlavni vlakno: Koncim!")

    # prvni_vlakno.join()
    # druhe_vlakno.join()
    logging.info("Hlavni vlakno: Az ted je doopravdy konec!")
