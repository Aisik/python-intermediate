# Vlakna (Threads - threading)
# - nove vlakno je vytvoreno v ramci existujiciho procesu (naseho pusteneho programu)
# - vytvoreni/spusteni vlakna je rychlejsi, nez vytvoreni noveho procesu
# - semafory (zamky) jsou casto potreba pro kontrolu pristupu k sdilenym datum
# - jeden GIL (Global Interpreter Lock) pro vsechny vlakna dohromady

# Vice procesu (Multiprocessing)
# - kdyz je novy proces spusten, je nezavisly na predchozim procesu
# - vytvoreni/spusteni procesu je pomalejsi nez pusteni vlakna
# - pamet neni sdilena mezi procesy
# - semafory (zamky) nejsou potreba (pokud nepouzivame multithreading uvnitr noveho procesu)
# - jeden GIL (Global Interpreter Lock) pro kazdy proces zvlast
