import timeit
import requests


from a_ukazka_vlaken import logger


def stahni_obsah_z_webu(url, cilovy_soubor):
    try:
        vysledek = requests.get(url).text
        with open(cilovy_soubor, "a") as soubor:
            soubor.write(vysledek)

    except Exception:
        logger.info("Nastala chyba pri stahovani obsahu z webu!")


def funkce_bez_vlaken(seznam_url):
    for url in seznam_url:
        stahni_obsah_z_webu(url, "bez_vlaken.txt")


def funkce_s_vlakny(seznam_url):
    import threading

    seznam_vlaken = []
    for url in seznam_url:
        vlakno = threading.Thread(target=stahni_obsah_z_webu, args=(url, "s_vlakny.txt"))
        vlakno.start()
        seznam_vlaken.append(vlakno)

    for vlakno in seznam_vlaken:
        vlakno.join()


if __name__ == "__main__":
    volani_bez_vlaken = "funkce_bez_vlaken(seznam_url)"
    volani_s_vlakny = "funkce_s_vlakny(seznam_url)"

    setup = """
from __main__ import funkce_bez_vlaken, funkce_s_vlakny

seznam_url = [
    "https://jsonplaceholder.typicode.com/todos/1",
    "https://jsonplaceholder.typicode.com/todos/2",
    "https://jsonplaceholder.typicode.com/todos/3"
]
    """

    # zpracovani s vlakny je mnohem rychlejsi, protoze zatimco jedno vlakno stahuje z webu nebo zapisuje do souboru,
    # muze se vykonavat logika jineho vlakna, treba otevirani souboru nebo zpracovani textu z webu
    logger.info(f"Bez vlaken: {timeit.timeit(stmt=volani_bez_vlaken, setup=setup, number=100)}")
    logger.info(f"S vlakny: {timeit.timeit(stmt=volani_s_vlakny, setup=setup, number=100)}")
