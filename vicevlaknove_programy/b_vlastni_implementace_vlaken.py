import time

from a_ukazka_vlaken import logger

import threading


vlakno_dva_pokracovani = True


class VlaknoSNavratovouHodnotou(threading.Thread):
    def __init__(self, target, args=(), kwargs=None):
        if kwargs is None:
            kwargs = {}

        self.target = target
        self.args = args
        self.kwargs = kwargs
        super().__init__()

    def run(self):
        self.vysledek = self.target(*self.args, **self.kwargs)

    def join(self, timeout=None):
        super().join(timeout)
        return self.vysledek


def soucet_x(nazev_vlakna, x):
    logger.info(f"Vlakno {nazev_vlakna}: Zacinam vypocet!")
    mezisoucet = 0
    for i in range(x):
        time.sleep(0.01)
        mezisoucet += i
    logger.info(f"Vlakno {nazev_vlakna}: Koncim vypocet!")
    return mezisoucet


def nekonecny_tisk(nazev_vlakna):
    while vlakno_dva_pokracovani:
        logger.info(f"Vlakno {nazev_vlakna}: Stale bezim!")
        time.sleep(1)


if __name__ == "__main__":
    logger.info("Vytvareni vlakna!")
    vlakno = VlaknoSNavratovouHodnotou(target=soucet_x, args=(1, 1000))
    vlakno_dva = threading.Thread(target=nekonecny_tisk, args=(2, ))
    vlakno.start()
    vlakno_dva.start()
    logger.info("Vlakno vytvoreno, cekam na dokonceni vypoctu!")
    vysledek = vlakno.join()
    logger.info(f"Vysledek: {vysledek}")
    # vlakno dva by bezelo do nekonecna, pokud bychom neprepnuli tento boolean na False
    # toto je dobra ukazka toho, ze promenne jsou mezi vlakny sdilene - pozdeji si ukazeme, ze napr. u multiprocessingu
    # tomu tak neni.
    vlakno_dva_pokracovani = False
    vlakno_dva.join()
    logger.info("Uplny konec!")





