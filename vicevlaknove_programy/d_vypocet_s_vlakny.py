import timeit


from a_ukazka_vlaken import logger


def odpocet(od, do):
    while od >= do:
        od -= 1


def funkce_bez_vlaken():
    odpocet(4000_000, 0)


def funkce_s_vlakny():
    import threading

    vlakno_jedna = threading.Thread(target=odpocet, args=(4000_000, 2000_000))
    vlakno_dva = threading.Thread(target=odpocet, args=(2000_000, 0))

    vlakno_jedna.start()
    vlakno_dva.start()

    vlakno_jedna.join()
    vlakno_dva.join()


if __name__ == "__main__":
    volani_bez_vlaken = "funkce_bez_vlaken()"
    volani_s_vlakny = "funkce_s_vlakny()"
    setup = "from __main__ import funkce_bez_vlaken, funkce_s_vlakny"

    # zde jsou vlakna pomalejsi, protoze neprovadime zadnou I/O (input/output - vstupne vystupni) operaci, takze jsou
    # vlakna brzdena zamkem GIL (Global Interpreter Lock) a vzdy bezi jen jedno. Jen si navrh pridame rezii spojenou
    # s vytvarenim a ukoncovanim vlaken
    # GIL povoluje beh maximalne jednoho vlakna najednou a jedinou vyjimkou jsou I/O operace, ktere timto zamkem (GIL)
    # nejsou blokovany. Proto je vhodne vyuzit vlakna pro optimalizaci pouze pokud delame nejake I/O operace, kterymi
    # jsou treba sitova komunikace nebo cteni/zapis z/do souboru.
    logger.info(f"Bez vlaken: {timeit.timeit(stmt=volani_bez_vlaken, setup=setup, number=100)}")
    logger.info(f"S vlakny: {timeit.timeit(stmt=volani_s_vlakny, setup=setup, number=100)}")
