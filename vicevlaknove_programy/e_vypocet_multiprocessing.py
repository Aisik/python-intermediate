import timeit


from a_ukazka_vlaken import logger
from d_vypocet_s_vlakny import odpocet


def funkce_multiprocessing():
    import multiprocessing

    proces_jedna = multiprocessing.Process(target=odpocet, args=(4000_000, 2000_000))
    proces_dva = multiprocessing.Process(target=odpocet, args=(2000_000, 0))

    proces_jedna.start()
    proces_dva.start()

    proces_jedna.join()
    proces_dva.join()


if __name__ == "__main__":
    volani_bez_vlaken = "funkce_bez_vlaken()"
    volani_s_vlakny = "funkce_s_vlakny()"
    volani_multiprocessing = "funkce_multiprocessing()"
    setup = """
from d_vypocet_s_vlakny import funkce_bez_vlaken, funkce_s_vlakny
from __main__ import funkce_multiprocessing
    """

    # ve finalnim porovnani si muzeme vsimnout, ze multiprocessing je nejrychlejsi, protoze vytvorime doslova novy
    # proces, cimz v podstate obejdeme GIL - ten je pro kazdy proces zvlast, takze procesor muze zpracovavat vice
    # nasich procesu doopravdy najednou
    # i tak zrychleni ale neni presnym nasobkem poctu procesu, protoze je zde navic nejaka rezie s vytvarenim procesu,
    # ukoncovanim procesu, atd.
    logger.info(f"Bez vlaken: {timeit.timeit(stmt=volani_bez_vlaken, setup=setup, number=100)}")
    logger.info(f"S vlakny: {timeit.timeit(stmt=volani_s_vlakny, setup=setup, number=100)}")
    logger.info(f"Multiprocessing: : {timeit.timeit(stmt=volani_multiprocessing, setup=setup, number=100)}")
