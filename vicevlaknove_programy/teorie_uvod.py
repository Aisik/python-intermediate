# vysvetleni:
#  A thread is a separate flow of execution. This means that your program will have two things happening at once.
#  But for most Python 3 implementations the different threads do not actually execute at the same time: they
#  merely appear to.

# Cilem je nejak zrychlit nas program. V dnesni dobe mame vykonne procesory, kazdy ma nekolik jader, kazde jadro
# ma typicky nekolik vlaken (2). My bychom chteli je umet vyuzivat.

# Getting multiple tasks running simultaneously requires a non-standard implementation of Python, writing some of your
# code in a different language, or using multiprocessing which comes with some extra overhead.
