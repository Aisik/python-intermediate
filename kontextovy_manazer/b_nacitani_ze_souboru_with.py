import json
import matplotlib.pyplot as plt

MILION = 1_000_000


def ziskej_hodnotu_populace_za_kazdy_rok(populace):
    populace_prehled_let = {}
    for rocni_data in populace:
        populace_prehled_let[rocni_data['date']] = rocni_data['value']
    return populace_prehled_let


def vykresli_populaci_do_grafu(cz_populace_prehled):
    serazeny_prehled_podle_roku = {
        rok: velikost_populace / MILION
        for rok, velikost_populace in sorted(cz_populace_prehled.items()) if int(rok) % 3 == 0
    }
    plt.plot(list(serazeny_prehled_podle_roku.keys()), list(serazeny_prehled_podle_roku.values()))
    plt.show()


if __name__ == "__main__":
    # pokud provadime nejake operace, ktere vyzaduji ukoncujici operace, je vzdy lepsi pouzit kontextovy manazer
    # nemuze se nam tak stat, ze zapomeneme na 'finally'
    with open("cz_populace.json") as soubor_cz_populace:
        try:
            cz_populace = json.load(soubor_cz_populace)
            cz_populace_prehled = ziskej_hodnotu_populace_za_kazdy_rok(cz_populace[1])
            vykresli_populaci_do_grafu(cz_populace_prehled)
        except Exception:
            print("Neco se nepovedlo!")
