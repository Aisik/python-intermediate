from contextlib import contextmanager
import json


# tento zpusob je mnohem kratsi a pokud tedy implementujeme nejaky velmi jednoduchy kontextovy manazer, tak je to
# vhodnejsi zpusob
@contextmanager
def muj_souborovy_manazer(cesta_k_souboru, mod="r"):
    soubor = open(cesta_k_souboru, mod)

    yield soubor

    soubor.close()


with muj_souborovy_manazer("cz_populace.json") as soubor_cz_populace:
    cz_populace = json.load(soubor_cz_populace)
    print(json.dumps(cz_populace, indent=4))
