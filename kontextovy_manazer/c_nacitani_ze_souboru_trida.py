import json


class MujSouborovyKontextManazer:
    """ Vlastni kontextovy manazer pomoci tridy. """
    def __init__(self, cesta_k_souboru, mod="r"):
        self.cesta_k_souboru = cesta_k_souboru
        self.mod = mod

    # v tele definujeme, co se ma stat pri vytvareni kontextu
    def __enter__(self):
        self.soubor = open(self.cesta_k_souboru, self.mod)
        return self.soubor

    # zde definujeme, co se ma stat pri opousteni kontextu
    # vyjmenovane parametry jsou povinne, pokud probehne vse bez chyby, jejich hodnoty jsou None, pokud nastane nejaka
    # print vyjimka, jsou naplneny informacemi o vyjimce
    # metoda by nemela nic vracet, predevsim ne True, jinak by vyjimka byla ignorovana!!!!
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.soubor.close()


with MujSouborovyKontextManazer("cz_populace.json") as soubor_cz_populace:
    cz_populace = json.load(soubor_cz_populace)
    print(json.dumps(cz_populace, indent=4))
