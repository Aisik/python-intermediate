import json


# https://project-awesome.org/jdorfman/awesome-json-datasets#population
soubor_cz_populace = open("cz_populace.json")


try:
    cz_populace = json.load(soubor_cz_populace)
    print(json.dumps(cz_populace, indent=4))
except Exception:
    print("Nepovedlo se nacist prehled populace CZ!")
finally:
    # zde provedeme operaci, ktera se musi provest vzdy, nehlede na to, jestli se stane vyjimka nebo ne
    # je to velmi neprekticke, protoze lehce muzeme zapomenout na to, ze musime udelat nejake 'finally'
    soubor_cz_populace.close()
