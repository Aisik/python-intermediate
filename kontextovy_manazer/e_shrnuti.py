import json
from datetime import datetime, timedelta
from pathlib import Path

from b_nacitani_ze_souboru_with import vykresli_populaci_do_grafu, ziskej_hodnotu_populace_za_kazdy_rok

MILION = 1_000_000


# Zde je vytvoren kontextovy manazer pomoci tridy, protoze bylo potreba implementovat dodatecnou metodu, ktera se tyka
# spravy kontextu otevreneho souboru
class MujSouborovyKontextManazer:
    def __init__(self, cesta_k_souboru, mod="r"):
        self.cesta_k_souboru = cesta_k_souboru
        self.mod = mod

    def __enter__(self):
        self.soubor = open(self.cesta_k_souboru, self.mod)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.soubor.close()

    def je_soubor_starsi_nez_1_rok(self):
        posledni_modifikace_cas_unix = Path(self.cesta_k_souboru).stat().st_mtime
        posledni_modifikace_datum_cas = datetime.fromtimestamp(posledni_modifikace_cas_unix)
        if datetime.now() > posledni_modifikace_datum_cas + timedelta(days=365):
            return True
        return False


with MujSouborovyKontextManazer("cz_populace.json") as soubor_cz_populace:
    if soubor_cz_populace.je_soubor_starsi_nez_1_rok():
        print("Soubor s daty je zastaraly!")
        exit()
    cz_populace = json.load(soubor_cz_populace.soubor)
    prehled_cz_populace = ziskej_hodnotu_populace_za_kazdy_rok(cz_populace[1])
    vykresli_populaci_do_grafu(prehled_cz_populace)
