"""
Nekterych veci muzeme dosahnout s pouzitim dekoratoru i kontextovych manazeru, je tedy
mozne je nekdy zamenit, pokud potrebujeme.

Vesmes ale kontextovy manazer slouzi k udrzovani nejakeho kontextu a predevsim __exit__ je zavolano vzdy!!
To u dekoratoru nemame zaruceno.

Jelikoz kontextvoy manazer muzeme definovat jako tridu, je taky jednodussi pridat kontextove metody.

Dekorator je vice svazany s konkretni funkci/metodou.
"""
import time
import contextlib


@contextlib.contextmanager
def doba_behu_kontextovy_manazer():
    zacatek = time.time()
    yield
    print(f"Beh funkce zabral: {time.time() - zacatek}")


def dlouhy_vypocet():
    for _ in range(20):
        time.sleep(0.1)


def doba_behu_dekorator(funkce):
    def wrapper():
        zacatek = time.time()
        funkce()
        print(f"Beh funkce zabral: {time.time() - zacatek}")
    return wrapper


@doba_behu_dekorator
def dlouhy_vypocet_s_dekoratorem():
    for _ in range(20):
        time.sleep(0.1)



dlouhy_vypocet_s_dekoratorem()

with doba_behu_kontextovy_manazer():
    dlouhy_vypocet()
