from functools import wraps
from typing import get_type_hints
import inspect


def type_check(func):
    # Retrieve the type hints of the function arguments
    hints = get_type_hints(func)

    @wraps(func)
    def wrapper(*args, **kwargs):
        bound_arguments = func.__annotations__

        # Check types of positional arguments
        for i, (arg, hint) in enumerate(zip(args, hints.values())):
            if not isinstance(arg, hint):
                raise TypeError(f"Argument {i} is of type {type(arg).__name__} but expected {hint.__name__}")

        # Check types of keyword arguments
        for arg, val in kwargs.items():
            if arg in hints and not isinstance(val, hints[arg]):
                raise TypeError(f"Argument {arg} is of type {type(val).__name__} but expected {hints[arg].__name__}")

        return func(*args, **kwargs)

    return wrapper


# Example usage
@type_check
def greeting(name: str, age: int) -> str:
    """ greeting docs """
    return f"Hello, {name}. You are {age} years old."


print(greeting("Alice", age=30))  # Works as expected
# print(greeting("Bob", "thirty"))  # Raises a TypeError
print(greeting.__name__)
print(greeting.__doc__)
