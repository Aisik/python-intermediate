"""
Druha cast je tedy dalsi dobrovolny domaci ukol, na ktery budete potrebovat regularni vyraz. Omlouvam se za jeho pozdnejsi zadani, ale mel by snad by pomerne jednoduchy a projdeme ho spolu i v nedeli v ramci procvicovani:
Napiste funkci s nazvem extrahuj_datum_z_url(url), ktere se preda url na jakykoliv clanek washington post a z daneho url se vyparsuje datum, tedy rok, mesic a den. Prikladem jsou tyto 3 url:
https://www.washingtonpost.com/national-security/2024/04/04/world-central-kitchen-us-weapons-israel/
https://www.washingtonpost.com/news/football-insider/wp/2016/09/02/odell-beckhams-fame-rests-on-one-stupid-little-ball-josh-norman-tells-author/
https://www.washingtonpost.com/national-security/2024/03/29/us-china-taiwan-marines/
Muzete si vsimnout ze priblizne uprostred danych url je i datum v tomto formatu YYYY/MM/DD, tedy napriklad takto:
2024/04/04
2016/02/9
2024/3/29
Jak dane datum vratite je v celku na vas, staci mi to napr. nejak takto:
(2024, 4, 4)
(2016, 9, 2)
(2024, 3, 29)
Vase reseni mi prosim poslete do zpravy a ja vam jej zkontroluju. Pokud nebude stihat, projdeme to spolu vsichni v nedeli. :slightly_smiling_face:
"""
# napiste funkci, ktera extrahuje datum z url:
# https://www.washingtonpost.com/national-security/2024/04/04/world-central-kitchen-us-weapons-israel/
# https://www.washingtonpost.com/news/football-insider/wp/2016/09/02/odell-beckhams-fame-rests-on-one-stupid-little-ball-josh-norman-tells-author/
# https://www.washingtonpost.com/national-security/2024/03/29/us-china-taiwan-marines/
import re


def extrahuj_url(url):
    re_date = re.compile(
        r"""
        /
        (\d{4})     # year
        /
        (\d{2})     # month
        /
        (\d{2})     # day
        /
        """, re.VERBOSE)
    match = re_date.search(url).groups()
    if match is not None:
        return tuple([int(date_part) for date_part in match])


url = "https://www.washingtonpost.com/news/football-insider/wp/2016/09/02/odell-beckhams-fame-rests-on-one-stupid-little-ball-josh-norman-tells-author/"
extrahovane_url = extrahuj_url(url)
print(extrahovane_url)


# # napiste program, ktery zmeni datum z formatu YYY-MM-DD na DD-MM-YYYY format
# def zmen_format_datumu(dt):
#     return re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', dt)
#
#
# datum = "2026-01-02"
# print("Originalni datum ve formatu YYY-MM-DD Format: ", datum)
# print("Nove datum ve formatu DD-MM-YYYY Format: ", zmen_format_datumu(datum))
#
#
# # alternativa nevyuzivajici regularni vyrazy
# from datetime import datetime
#
#
# stare_datum = datetime.strptime(datum, "%Y-%m-%d")
# print(stare_datum)
# nove_datum = stare_datum.strftime("%d-%m-%Y")
# print(nove_datum)
