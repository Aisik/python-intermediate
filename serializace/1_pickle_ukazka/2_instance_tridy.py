# muzeme takto ukladat i instance tridy

import pickle
from dataclasses import dataclass


@dataclass
class Uzivatel:
    jmeno: str
    prijmeni: str


testovaci_uzivatel = Uzivatel("Pavel", "Eis")
print(testovaci_uzivatel)

nazev_pick_souboru = "uzivatel.pick"
with open(nazev_pick_souboru, 'wb') as soubor_pickle:
    pickle.dump(testovaci_uzivatel, soubor_pickle)

with open(nazev_pick_souboru, 'rb') as soubor_pickle:
    nacteny_uzivatel = pickle.load(soubor_pickle)

print(nacteny_uzivatel)
