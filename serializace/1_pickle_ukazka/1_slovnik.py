# pomoci modulu pickle muzeme serializovat (ukladat v binarni podobe) objekty jazyka Python, napr. slovnik

import pickle

data = {
    'jmeno': "Petr",
    'prijmeni': "Novak",
    'dovednosti': ["python", "java", "sql"]
}


# ulozime data do souboru 'data.pick' a pote je z nej nacteme zpet a overime printem
nazev_pick_souboru = "data.pick"
with open(nazev_pick_souboru, "wb") as soubor_pick:
    pickle.dump(data, soubor_pick)

with open(nazev_pick_souboru, "rb") as soubor_pick:
    nactena_data = pickle.load(soubor_pick)

print(nactena_data)
