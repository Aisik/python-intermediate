from dataclasses import dataclass
import pickle


@dataclass
class Uzivatel:
    jmeno: str
    prijmeni: str
    vek: int


with open("uzivatel.pick", "rb") as soubor_pickle:
    nacteny_uzivatel = pickle.load(soubor_pickle)

print(nacteny_uzivatel)


# Toto nefunguje, protoze jsme pridali atribut vek do tridy Uzivatel. Ale kdyz mame ulozen objekt binarne, binarne je
# ulozena i trida, ktere je objekt instanci. Pokud se zmeni jeji struktura (predpis), nelze jeji objekt znovu nacist
# z binarni podoby.

# Resenim je neukladat data primo jako jejich binarni reprezentaci, ktera je dana programem, ale ukladat pouze data,
# z kterych muzeme jednoduse zrekonstruovat objekt, v tomto pripade ulozit jen jmeno a prijmeni, vek muzeme v nasich
# datech doplnit, typicky se k tomu pouziva format JSON nebo CSV
