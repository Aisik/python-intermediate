# https://project-awesome.org/jdorfman/awesome-json-datasets
# https://api.nobelprize.org/v1/prize.json
import requests
import json


odpoved_seznam_nobelovych_cen = requests.get("https://api.nobelprize.org/v1/prize.json")
seznam_nobelovych_cen = json.loads(odpoved_seznam_nobelovych_cen.text)

seznam_kategorii = ["chemistry", "economics", "literature", "peace", "physics", "medicine"]
prehled_kategorii = {}
for kategorie in seznam_kategorii:
    prehled_kategorii[kategorie] = len(
        [cena for cena in seznam_nobelovych_cen['prizes'] if cena["category"] == kategorie]
    )

print(json.dumps(seznam_nobelovych_cen, indent=4))
print(json.dumps(prehled_kategorii, indent=4))

with open("nobelovka_prehled_kategorii.json", "w") as soubor_prehled_kategorii:
    json.dump(prehled_kategorii, soubor_prehled_kategorii, indent=4, sort_keys=True)
