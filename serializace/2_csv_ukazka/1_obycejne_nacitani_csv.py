seznam_zamestnancu = []

with open("zamestnanci.csv") as soubor_zamestnancu:
    for radek in soubor_zamestnancu:
        # musim volat strip(), jinak bych mel nactene \n
        informace_o_zamestnanci = [zamestnanec_info.strip() for zamestnanec_info in radek.split(',')]
        seznam_zamestnancu.append(informace_o_zamestnanci)

print(seznam_zamestnancu)
