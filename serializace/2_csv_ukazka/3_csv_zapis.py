import csv

with open('auta.csv', mode='w') as soubor_auta:
    auta_zapis = csv.writer(soubor_auta, delimiter=',')

    auta_zapis.writerow(["Ford", "Focus"])
    auta_zapis.writerow(["Skoda", "Octavia"])
    auta_zapis.writerow(["Citroen", "Berlingo"])
