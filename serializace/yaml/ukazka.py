import yaml
import json


with open("data_basic.yml") as soubor_yml:
    nactena_data = yaml.safe_load(soubor_yml)

print(json.dumps(nactena_data, indent=4))
