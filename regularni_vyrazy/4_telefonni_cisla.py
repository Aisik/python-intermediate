# Snazime se rozdelit americke telefonni cislo na 4 casti:
#    - kod oblasti  (3 cisla)
#    - hlavni linka (3 cisla)
#    - zbytek cisla (4 cisla)
#    - volitelne rozsireni cisla (4 cisla)

# Napr. chceme rozlozit cislo 800-555-1212 na kod oblasti (800), hlavni linka (555) a zbytek cisla (1212)
# Co vse bychom meli umet spravne nacist??
#  800-555-1212
#  800 555 1212
#  800.555.1212
#  (800) 555-1212
#  1-800-555-1212
#  800-555-1212-1234
#  800-555-1212x1234
#  800-555-1212 ext. 1234
#  work 1-(800) 555.1212 #1234


class TestovacRegexu:
    def __init__(self):
        self._poradnik = self._dalsi_cislo()

    @staticmethod
    def _dalsi_cislo():
        cislo = 1
        while True:
            yield cislo
            cislo += 1

    def otestuj_regex(self, regex, testovany_retezec):
        vysledek = regex.search(testovany_retezec)
        if vysledek:
            self._vytiskni_vysledek(testovany_retezec, vysledek.groups())
        else:
            self._vytiskni_vysledek(testovany_retezec, vysledek)

    def vytikni_novou_sekci(self, regex):
        print("--------------------------------------")
        print(regex)

    def _vytiskni_vysledek(self, prohledavany_retezec, vysledek):
        prefix_vypisu = f"[{next(self._poradnik)}] "
        print(f"{prefix_vypisu}{prohledavany_retezec}")
        print(f"{' ' * len(prefix_vypisu)}{vysledek}")


# Pro tento problem budeme muset urcite pouzit regularni vyrazy:
import re


tester = TestovacRegexu()
re_tel_cislo = re.compile(r"^(\d{3})-(\d{3})-(\d{4})$")
tester.vytikni_novou_sekci(re_tel_cislo.pattern)
# ukazme si groups
tester.otestuj_regex(re_tel_cislo, "800-555-1212")
# zatim vubec nematchujeme extensions
tester.otestuj_regex(re_tel_cislo, "800-555-1212-1234")


# rozsirime nas regularni vyraz o extension:
re_tel_cislo = re.compile(r"^(\d{3})-(\d{3})-(\d{4})-(\d+)$")
tester.vytikni_novou_sekci(re_tel_cislo.pattern)
tester.otestuj_regex(re_tel_cislo, "800-555-1212-1234")
# uz dokazeme matchovat vsechny 4 casti, ale nyni pozadujeme vzdy 4 casti (extension je ale nepovinna)
tester.otestuj_regex(re_tel_cislo, "800-555-1212")
# navic oddelovace mezi castmi cisla mohou byt i mezery, to by nam taky zatim nefungovalo
tester.otestuj_regex(re_tel_cislo, "800 555 1212 1234")


# zmenili jsme oddelovat casti cisla z '-' na '\D+'
re_tel_cislo = re.compile(r"^(\d{3})\D+(\d{3})\D+(\d{4})\D+(\d+)$")
tester.vytikni_novou_sekci(re_tel_cislo.pattern)
tester.otestuj_regex(re_tel_cislo, "800 555 1212 1234")
tester.otestuj_regex(re_tel_cislo, "800-555-1212-1234")
# cislo ale nemusi mit vubec zadne oddelovace
tester.otestuj_regex(re_tel_cislo, "80055512121234")
# a furt jsme nevyresili to, ze extension by mel byt volitelny
tester.otestuj_regex(re_tel_cislo, "800-555-1212")


# zmenili jsme oddelovace z '\D+' --> '\D*' a na konci jsme udelali extension nepovinnou + --> *
re_tel_cislo = re.compile(r"^(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$")
tester.vytikni_novou_sekci(re_tel_cislo.pattern)
tester.otestuj_regex(re_tel_cislo, "80055512121234")
tester.otestuj_regex(re_tel_cislo, "800.555.1212 x1234")
tester.otestuj_regex(re_tel_cislo, "800-555-1212")
# jenze na zacatku muze byt kod oblasti obalen kulatymi zavorkami
tester.otestuj_regex(re_tel_cislo, "(800)5551212 x1234")


# na zacatek regularniho vyrazu dame '\D*', abychom rekli, ze cislo muze zacit cim chce:
re_tel_cislo = re.compile(r"^\D*(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$")
tester.vytikni_novou_sekci(re_tel_cislo.pattern)
tester.otestuj_regex(re_tel_cislo, "(800)5551212 ext. 1234")
tester.otestuj_regex(re_tel_cislo, "800-555-1212")
# na zacatku regexu mame '\D*', ale to matchuje neciselne znaky, tady je 'work 1-'
tester.otestuj_regex(re_tel_cislo, "work 1-(800) 555.1212 #1234")


# proto je potreba posledni uprava, kdy uz nekontrolujeme cislo uplne od zacatku:
re_tel_cislo = re.compile(r"(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$")
tester.vytikni_novou_sekci(re_tel_cislo.pattern)
tester.otestuj_regex(re_tel_cislo, "(800)5551212 ext. 1234")
tester.otestuj_regex(re_tel_cislo, "800-555-1212")
tester.otestuj_regex(re_tel_cislo, "80055512121234")


# HUUUURAY, tohle uz funguje, ted uz jen pro prehlednost rozepsat na vice radku:
re_tel_cislo = re.compile(r"""
               # nevázat se na začátek řetězce, číslo může začít kdekoliv
    (\d{3})    # číslo oblasti má 3 číslice (např. '800')
    \D*        # nepovinný oddělovač - libovolný počet nenumerických znaků
    (\d{3})    # číslo hlavní linky má 3 číslice (např. '555')
    \D*        # nepovinný oddělovač
    (\d{4})    # zbytek čísla má 4 číslice (např. '1212')
    \D*        # nepovinný oddělovač
    (\d*)      # nepovinná klapka - libovolný počet číslic
    $          # konec řetězce
    """, re.VERBOSE
)
tester.vytikni_novou_sekci(re_tel_cislo.pattern)
tester.otestuj_regex(re_tel_cislo, "work 1-(800) 555.1212 #1234")
tester.otestuj_regex(re_tel_cislo, "800-555-1212")
