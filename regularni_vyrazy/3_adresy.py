# Mam seznam americkych adres, u kterych bych mel nahradit 'ROAD' za 'RD.'
# nebo treba 'STREET' za 'ST.'

# zaciname:
adresa = "100 NORTH MAIN ROAD"
vysledek = adresa.replace("ROAD", "RD.")
print(f"---\n[1]: {vysledek}")
# --> 100 NORTH MAIN RD.

# jsme hotovi, funguje to. Nooo, ne uplne:
adresa = "100 NORTH BROAD ROAD"
vysledek = adresa.replace("ROAD", "RD.")
print(f"---\n[2]: {vysledek}")
# --> 100 NORTH BRD. RD.

# zmenili jsme i BROAD na BRD. a to nechceme
# zkusime replace(count=1)
vysledek = adresa.replace("ROAD", "RD.", 1)
print(f"---\n[3]: {vysledek}")
# --> 100 NORTH BRD. ROAD

# my chceme ale nahradit ROAD a ne BROAD
# jsme odsouzeni k pouziti regularnich vyrazu :(
import re

vysledek = re.sub("ROAD$", "RD.", adresa)
print(f"---\n[4]: {vysledek}")

# tohle uz funguje. Ale jen nekdy, protoze ne kazda adresa obsahuje na konci "ROAD"
# a tehdy nechceme nahradit nic!
adresa = "100 BROAD"
vysledek = re.sub("ROAD$", "RD.", adresa)
print(f"---\n[5]: {vysledek}")


# pridame tedy ohraniceni na slovo pomoci \b (\\b)
vysledek = re.sub("\\bROAD$", "RD.", adresa)
print(f"---\n[6]: {vysledek}")
# kontrola predchoziho:
vysledek = re.sub("\\bROAD$", "RD.", "100 NORTH BROAD ROAD")
print(f"[7]: {vysledek}")
# stale funguje, dobry

# jenze prichazi dalsi adresa, kde "ROAD" uz neni na konci:
adresa = "100 BROAD ROAD APT. 3"
vysledek = re.sub("\\bROAD\\b", "RD.", adresa)
print(f"---\n[8]: {vysledek}")
# vypada to, ze to funguje, tak uz jen kontrola:
vysledek = re.sub("\\bROAD\\b", "RD.", "100 NORTH BROAD ROAD")
print(f"[9]: {vysledek}")
vysledek = re.sub("\\bROAD\\b", "RD.", "100 NORTH MAIN ROAD")
print(f"[10]: {vysledek}")
