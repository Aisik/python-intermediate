# https://docs.python.org/3/library/re.html
# https://regex101.com/
import re

# '.' znaci jakykoliv znak krome znaku noveho radku
match = re.search(".eta", "hele teta je tam")
print(match.string)
print(match.start())
print(match.end())
print(match.span())

match = re.search(".eta", "meta")
print(match.string)
match = re.search(".eta", "auto")
# kdyz nas regularni vyraz nematchne v retezci, vrati metoda search 'None'
print(match.string)

# metoda search hleda kdekoliv v retezci:
test_retezec = "Prijde dnes vecer teta?"
match = re.search(".eta", test_retezec)
print(match.span())
# kdyz bych chtel najit presny retezec, ktery jsem matchnul pomoci sveho regularniho vyrazu
print(test_retezec[match.start():match.end()])

# '?' = 0 nebo 1 znak, za kterym je '?'
test_retezec = "Koukam se skrz okno a vidim, ze prsi!"
match = re.search("okn?o", test_retezec)
print(test_retezec[match.start():match.end()])

# '+' = 1 nebo vice znaku, za kterym je '+'
test_retezec = "Vyhral jsem ve sportce. Huraaaaa!"
match = re.search("Hura+", test_retezec)
print(test_retezec[match.start():match.end()])
# '*' = stejne jako '+', jen minimum je 0 (misto 1)

# '[]' = pomoci hranatych zavorek muzeme na jeden znak dat vice moznosti
test_retezec = "Petr spadl!"
match = re.search("^[PM]etr", test_retezec)
print(test_retezec[match.start():match.end()])

# muzeme to vyuzit pro matchovani vsech cislic:
test_retezec = "Pavel dostal 3 bod!"
match = re.search("[1234567890] bod", test_retezec)
print(test_retezec[match.start():match.end()])
# muzeme zlepsit na --> '[0-9]'
# nebo dokonce na '\d'

# muzeme pridavat i kvantifikatory:
test_retezec = "Jirka vyhral 200000 korun"
match = re.search("vyhral \d+ ", test_retezec)
print(test_retezec[match.start():match.end()])
# \s = jakykoliv bily znak = mezera, tabulator, novy radek
# \w = [A-Za-z0-9_]

# '{cislo}' = urci presny pocet znaku (nebo skupin znaku)
# '{minimum, maximum}' = urci mozny presny rozsah poctu opakovani
test_retezec = "8.8.8.8"
match = re.search("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", test_retezec)
print(test_retezec[match.start():match.end()])

# '^' = znamena, ze chceme matchovat od zacatku retezce
test_retezec = "Petr bezi domu."
test_retezec_dva = "Bezi uz Petr domu?"
test_retezec_tri = "Petr beziii!"
match = re.search("^Petr bezi+", test_retezec)
print(test_retezec[match.start():match.end()])
# '$' = znamena konec retezce

# co kdyz chceme v retezci najit znak '.'? Musime pouzit '\' (escape sekvenci)
test_retezec = "nejaky nahodny text a jeho email je pavel.eis@cesnet.cz"
match = re.search("@(oracle\.com|cesnet\.cz)$", test_retezec)
print(test_retezec[match.start():match.end()])

# re.match()
# bere stejne parametry jako search, jen hleda od zacatku retezce!
# takze neco podobneho jako search, kdybychom u kazdeho patternu meli '^'

# re.findall()
# vrati seznam vsech matchu, ale jako retezce, ne jako match object
# vrat vsechny slova, ktera zacinaji na pismeno
test_retezec = "seznam najdu tam co neznam"
# \b backspace - je vzdy dobre pouzit r - raw string
seznam_nalezenych_slov = re.findall(r"\bn[a-z]*", test_retezec)
print(seznam_nalezenych_slov)
# pokud bychom chteli match objekty, muzeme pouzit funkci re.finditer(), ktera funguje stejne jako re.findall(),
# pokud postupne iterujeme, dostavame zpet primo match objekty, ne jen retezce


# re.split() funguje velmi podobne "retezec".split(), ale muzeme primo preda regularni vyraz, podle ktereho chceme
# retezec rozdelit
csv_retezec = "jablko, hruska, pomeranc; mrkev,zeli,banan  ; boruvky"
seznam_ovoce_a_zeleniny = re.split(r"\s*[,;]\s*", csv_retezec)
print(seznam_ovoce_a_zeleniny)

# re.sub() - zmeni vsechny podretezce popsane regularnim vyrazem v retezci na specifikovany retezec
opraveny_csv_retezec = re.sub(r"\s*[,;]\s*", ",", csv_retezec)
print(opraveny_csv_retezec)

# re.subn() - funguje uplne stejne jako re.sub(), jen navic vrati jako druhou hodnotu pocet provedenych nahrazeni
opraveny_csv_retezec = re.subn(r"\s*[,;]\s*", ",", csv_retezec)
print(opraveny_csv_retezec)

# skupiny - jsou velmi uzitecne k tomu, abychom
# najdi v textu vsechny prirazeni
popis_stolu = "Rekneme, ze ten stul bychom mohli vyrobit tak, aby mel parametry " \
              "vyska = 150 cm, sirka = 140 cm a hloubka = 70 cm."
nalezene_parametry_stolu = re.findall(r"(\w+)\s?=\s?(\d+)", popis_stolu)
print(nalezene_parametry_stolu)




# re.fullmatch()
# vrati match objekt pouze pokud regularni vyraz pasuje na cely predavany retezec
# pokud regularni vyraz matchuje jen cast retezce, nedojde k matchi

# velmi jednoducha validace ceskych webovych url
test_retezec = "www.cesnet.cz"
match = re.fullmatch("(www\.)?\w*\.cz", test_retezec)
print(match)


# zajimave dodatecne parametry:
# re.IGNORECASE - pokud predame jakekoliv funkci pouzivajici regularni vyraz, bude ignorovana velikost pismen
# re.MULTILINE - vyhledava oddelene na radcich
# re.VERBOSE - umoznuje rozepsat regularni vyraz na vice radku
