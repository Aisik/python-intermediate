people = [
    {"name": "Alice", "age": 30, "city": "New York"},
    {"name": "Bob", "age": 25, "city": "Los Angeles"},
    {"name": "Charlie", "age": 35, "city": "Chicago"},
    {"name": "Diana", "age": 28, "city": "Miami"},
    ('test', 'of', 'immutability')
]

people_copy = people.copy()
people[0]['age'] = 31

print(people_copy)
print(people)

print(id(people_copy))
print(id(people))

from copy import deepcopy


people_copy = deepcopy(people)
...

# show it on function !!!