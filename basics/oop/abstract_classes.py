from pprint import pprint
from abc import ABC, abstractmethod


# expl start with just shodan, explaining staticmethod and classmethod. Then run Shodan. Then add Censys and
#   start with abstract class, why is it useful.


class Shodan:
    SHODAN_DOMAIN = "shodan.io`"

    def __init__(self, configuration: dict):
        self.shodan_data = self._get_connection_to_shodan(configuration["username"], configuration["password"])

    @staticmethod
    def _get_data_from_shodan() -> dict:
        return {
            "1.1.1.1": {
                "open_ports": [53, 69, 80, 161, 443, 2082, 2083, 2086, 2087, 2095, 8080, 8443, 8880],
                "hostname": "one.one.one.one",
            },
            "9.9.9.9": {"open_ports": [53, 443], "hostname": "dns.google"},
        }

    @classmethod
    def _get_connection_to_shodan(cls, username: str, password: str) -> dict:
        print(f"Connecting to {cls.SHODAN_DOMAIN} with these credentials: username={username}, password={password}")
        return cls._get_data_from_shodan()

    def get_info_about_ip_address_from_shodan(self, ip_address: str) -> dict:
        shodan_data = self.shodan_data
        try:
            found_data = shodan_data[ip_address]
            # found_data['data_source'] = "Shodan"
            return found_data
        except KeyError:
            return {}


class Censys:
    CENSYS_DOMAIN = "censys.io"

    def __init__(self, configuration: dict):
        self.censys_data = self._get_connection_to_censys(configuration["username"], configuration["password"])

    @staticmethod
    def _get_data_from_censys() -> dict:
        return {
            "1.1.1.1": {
                "open_ports": [53, 80, 443, 853],
                "hostname": "one.one.one.one",
            },
            "8.8.8.8": {"open_ports": [53, 443, 853], "hostname": "dns.google"},
        }

    @classmethod
    def _get_connection_to_censys(cls, username: str, password: str) -> dict:
        print(f"Connecting to {cls.CENSYS_DOMAIN} with these credentials: username={username}, password={password}")
        return cls._get_data_from_censys()

    def get_info_about_ip_address_from_censys(self, ip_address: str) -> dict:
        censys_data = self.censys_data
        try:
            found_data = censys_data[ip_address]
            # found_data['data_source'] = "Censys"
            return found_data
        except KeyError:
            return {}


def main():
    data_source_configuration = {
        "Shodan": {"username": "eis", "password": "strongestPasswordInTheWorld"},
        "Censys": {"username": "eis", "password": "StrongPassword"},
    }

    shodan = Shodan(data_source_configuration['Shodan'])
    censys = Censys(data_source_configuration['Censys'])

    list_of_ips = ["1.1.1.1", "8.8.8.8"]
    gathered_data = {}
    for ip_address in list_of_ips:
        gathered_data[ip_address] = {'shodan': shodan.get_info_about_ip_address_from_shodan(ip_address)}
        gathered_data[ip_address]['censys'] = censys.get_info_about_ip_address_from_censys(ip_address)

    pprint(gathered_data)


main()
