# expl ukazat proste postupne dedicnost a pote preklopit na kompozici s abstraktni tridou.


class Vozidlo:
    def __init__(self, znacka, model, rok_vyroby):
        self.znacka = znacka
        self.model = model
        self.rok_vyroby = rok_vyroby

    def nastartovat(self):
        print(f"{self.znacka} {self.model} z roku {self.rok_vyroby} startuje!")

    def vypnout_motor(self):
        print(f"{self.znacka} {self.model} z roku {self.rok_vyroby} vypina motor!")


class Auto(Vozidlo):
    def __init__(self, znacka, model, rok_vyroby, velikost_kufru):
        super().__init__(znacka, model, rok_vyroby)
        self.velikost_kufru = velikost_kufru

    def otevrit_kufr(self):
        print(f"Otevira se kufr vozu {self.model}, jehoz velikost je {self.velikost_kufru} l!")

    def zavrit_kufr(self):
        print(f"Zavira se kufr vozu {self.model}, jehoz velikost je {self.velikost_kufru} l!")


class ElektrickyMotor:
    def __init__(self, velikost_baterie):
        self.velikost_baterie = velikost_baterie

    def nabit_baterii(self):
        print(f"Baterie o velikosti {self.velikost_baterie} kWh se nabila.")


class ElektrickeAuto(Auto, ElektrickyMotor):
    def __init__(self, znacka, model, rok_vyroby, velikost_kufru, velikost_baterie):
        Auto.__init__(self, znacka, model, rok_vyroby, velikost_kufru)
        ElektrickyMotor.__init__(self, velikost_baterie)


tesla = ElektrickeAuto("Tesla", "Model 3", "2023", "500", "100")


