# seznam_cisel = [1, 2, 3]
#
# for cislo in seznam_cisel:
#     print(cislo)
#
# # expl nastane vyjimka, u seznamu to ale neni az tak caste
# # print(seznam_cisel[5])
#
#
# prehled_zamestnancu = {
#     'Petr Novak': {
#         'vek': 35,
#         'pozice': "udrzbar",
#         'trvale bydliste': "Brno"
#     },
#     'Lucie Pokorna': {
#         'vek': 25,
#         'pozice': "pravnik",
#         'trvale bydliste': "Breclav"
#     },
# }
#
# # expl casteji to nastane u slovniku
# print(prehled_zamestnancu['Petr Novak'])
# # expl vyhodi vyjimku
# # print(prehled_zamestnancu['Pavel Eis'])
#
# # expl jak to osetrit? Napada nekoho?
# hledana_osoba = "Pavel Eis"
# try:
#     print(prehled_zamestnancu['Pavel Eis'])
# except KeyError:
#     print(f"Hledana osoba {hledana_osoba} nebyla nalezena v databazi zamestnancu!")
#
# # expl vyjimku muzeme osetrit i jinak nezli blokem try-catch, napada nekoho jak?
# # hledana_osoba = "Petr Novak"
# # if hledana_osoba in prehled_zamestnancu:
# #     print(f"Zamestnanec byl nalezen, jeho pozice je: {prehled_zamestnancu[hledana_osoba]['pozice']}")
# # else:
# #     print(f"Hledana osoba {hledana_osoba} nebyla nalezena v databazi zamestnancu!")
#
#
# # expl Proc tam mame KeyError? Fungovalo by treba Exception? Fungoval.. zkusime tam dat i LookupError a ono to taky funguje
# # ukazat built in exceptions v pythonu
#
# # expl ukazal nejprve s Exception, proc chceme umet rozlisovat dane vyjimky
# def deleni(delenec, delitel):
#   try:
#     vysledek = delenec / delitel
#
#   except ZeroDivisionError:  # Catch specific exception first
#     print("Error: Cannot divide by zero.")
#   except TypeError:  # Catch more general exceptions later
#     print("Error: Invalid input for division.")
#   else:
#     print("Result:", vysledek)
#
# deleni(10, 2)  # Output: Result: 5.0
# deleni(10, 0)  # Output: Error: Cannot divide by zero.
# deleni("hello", 2)  # Output: Error: Invalid input for division.

# expl ukazka s finally, bude potreba pak vytvorit file, ukaz to pak s kontextovym manazerem
def read_file_content(filename):
    f = None  # Initialize f to None
    try:
        f = open(filename, 'r')
        content = f.read()
        print("File content read successfully.")
        return content
    except FileNotFoundError:
        print("Error: The file does not exist.")
    except OSError:
        print("Error: An IO error occurred while reading the file.")
    finally:
        if f:  # Check if f is not None (meaning the file was successfully opened)
            print("Closing the file.")
            f.close()


read_file_content('test.txt')