class NedostatekProstredkuError(Exception):
    """Vyjimka, ktera je vyhozena v pripade vyberu vetsiho mnozstvi penez, nez ktere je na ucte."""
    pass


class InvalidniOperaceError(Exception):
    """Exception raised for errors in the account operations due to invalid actions."""
    pass


class BankovniUcet:
    def __init__(self, pocatecni_zustatek=0):
        self.zustatek = pocatecni_zustatek

    def vlozit_penize(self, castka):
        if castka < 0:
            raise InvalidniOperaceError("Vkladana castka nemuze byt zaporna!")
        self.zustatek += castka

    def vybrat_penize(self, castka):
        if castka > self.zustatek:
            raise NedostatekProstredkuError(f"Nelze vybrat {castka}, protoze pouze {self.zustatek} je dostupnych!")
        elif castka < 0:
            raise InvalidniOperaceError("Nelze vybirat zapornou castku!")
        self.zustatek -= castka

    def zustatek(self):
        return self.zustatek


ucet = BankovniUcet(100)
try:
    ucet.vlozit_penize(-50)
except InvalidniOperaceError as e:
    print(f"Error: {e}")
ucet.vybrat_penize(150)
