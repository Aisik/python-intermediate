# nepouzite funkce jsou take zajimave - ale na vsechny neni prostor: https://docs.python.org/3/library/itertools.html
from itertools import islice, cycle, chain, accumulate

from b_nejjednodussi_reseni import nacti_prodeje


prodeje = nacti_prodeje("prodeje.csv")
# chceme pouze prodeje mezi 4000 a 5000 a chceme zacit az u 100. prodeje
# https://docs.python.org/3/library/itertools.html#itertools.islice
polozky = islice(filter(lambda cena_prodeje: 4000 < cena_prodeje < 5000, prodeje), 100)
for polozka in polozky:
    print(polozka)
