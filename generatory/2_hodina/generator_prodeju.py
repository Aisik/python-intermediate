"""
Vygeneruje CSV soubor, ktery reprezentuje prodeje ve forme 'date of sale, cost'. Slouzi jako zaklad dat pro priklad
s generatory.
"""

import random
from datetime import datetime
import csv


DATUM_FORMAT = "%Y-%m-%d"


def vygeneruj_nahodne_datum(rok_pocatek, rok_konec):
    nahodny_rok = random.randint(rok_pocatek, rok_konec)
    nahodny_mesic = random.randint(1, 12)
    if nahodny_mesic == 2:
        nahodny_den = random.randint(1, 28)
    elif nahodny_mesic == (1, 3, 5, 7, 8, 10, 12):
        nahodny_den = random.randint(1, 31)
    else:
        nahodny_den = random.randint(1, 30)
    nahodne_datum = datetime(year=nahodny_rok, month=nahodny_mesic, day=nahodny_den)
    return nahodne_datum.strftime(DATUM_FORMAT)


def vygeneruj_nahodou_cenu(max_cost):
    return random.randint(1, max_cost)


ROK_POCATEK = 2011
ROK_KONEC = 2021
MAXIMALNI_CENA = 20_000
with open("prodeje.csv", 'w') as prodeje_soubor:
    zapisovac = csv.writer(prodeje_soubor)
    zapisovac.writerow(["#Datum prodeje", "cena"])
    for _ in range(100):
        zapisovac.writerow([vygeneruj_nahodne_datum(ROK_POCATEK, ROK_KONEC), vygeneruj_nahodou_cenu(MAXIMALNI_CENA)])

