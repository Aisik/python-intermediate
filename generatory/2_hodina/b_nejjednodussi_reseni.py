import csv
import itertools


def nacti_prodeje(cesta_k_prodejum):
    with open(cesta_k_prodejum) as prodeje_soubor:
        nacitac = csv.reader(prodeje_soubor)
        next(nacitac)
        for radek in nacitac:
            yield int(radek[1])


def zpracuj_prodeje(prodeje):
    # itertools.tee rozdeli nasi puvodni itarovatelnou hodnotu (generator) na 2 nove. Kazdy nyni muzeme pouzit na
    # jiny druh iterace, ktery potrejume, aniz bychom museli delat tri iterace
    min_, max_, avg = itertools.tee(prodeje, 2)
    return min(min_), max(max_)


# Kdyz nad stejnym objektem cyklime vicekrat, je vhodne se zamyslet, jestli nam nepomuze itertools.tee
