import csv


def nacti_prodeje(cesta_k_prodejum):
    prodeje = []
    with open(cesta_k_prodejum) as prodeje_soubor:
        nacitac = csv.reader(prodeje_soubor)
        # precti prvni radek, ktery nas nezajima
        next(nacitac)
        for radek in nacitac:
            prodeje.append(int(radek[1]))
    return prodeje


class ProdejniStatistiky:
    def __init__(self, ceny_prodeju):
        # funkce iter() vytvori z listu iterator, takze nyni nad listem pude volat 'next()', coz
        # vyuzijeme v inicializaci
        self.ceny_prodeju = iter(ceny_prodeju)
        self.minimalni_prodej = None
        self.maximalni_prodej = None
        self._inicializace()

    def _inicializace(self):
        # slo by i bez iteratoru, ale toto je ukazka, jak jde pracovat i s iteratorem
        try:
            prvni_hodnota = next(self.ceny_prodeju)
        except StopIteration:
            raise ValueError("Nebyly predany zadne prodeje!")

        self.minimalni_prodej = self.maximalni_prodej = prvni_hodnota

    def zpracuj_prodeje(self):
        for cena_prodeje in self.ceny_prodeju:
            self._aktualizuj_minimum(cena_prodeje)
            self._aktualizuj_maximum(cena_prodeje)
        return self

    def _aktualizuj_minimum(self, nova_hodnota):
        if nova_hodnota < self.minimalni_prodej:
            self.minimalni_prodej = nova_hodnota

    def _aktualizuj_maximum(self, nova_hodnota):
        if nova_hodnota > self.maximalni_prodej:
            self.maximalni_prodej = nova_hodnota


prodeje = nacti_prodeje("prodeje.csv")
import sys
print(sys.getsizeof(prodeje))
statistiky = ProdejniStatistiky(prodeje)
statistiky.zpracuj_prodeje()
# Mohli bychom proste zavolat min() a max() nad prodeji, ale to by se seznam prosel dvakrat
print(statistiky.maximalni_prodej)
print(statistiky.minimalni_prodej)
