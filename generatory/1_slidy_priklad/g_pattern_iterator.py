class PatternIterator:
    def __init__(self, items):
        self.items = items
        # Defining the pattern as per the given example
        # For a list [1, 2, 3, 4, 5], the pattern is [0, 4, 1, 3, 2]
        self.pattern = [0, len(items) - 1]
        for i in range(1, (len(items) + 1) // 2):
            self.pattern.append(i)
            if len(self.items) - i - 1 != i:
                self.pattern.append(len(self.items) - i - 1)
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.index < len(self.pattern):
            result = self.items[self.pattern[self.index]]
            self.index += 1
            return result
        else:
            raise StopIteration


my_list = [1, 2, 3, 4, 5]
random_iterator = PatternIterator(my_list)

for element in random_iterator:
    print(element)
