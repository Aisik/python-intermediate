from math import sqrt
import sys


def je_cislo_prvocislo(n):
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True


def ziskej_n_prvocisel(n):
    prvocisla = []
    aktualni_cislo = 2
    while len(prvocisla) != n:
        if je_cislo_prvocislo(aktualni_cislo):
            prvocisla.append(aktualni_cislo)
        aktualni_cislo += 1
    return prvocisla


if __name__ == "__main__":
    seznam_prvocisel = ziskej_n_prvocisel(100_000)
    print(sys.getsizeof(seznam_prvocisel))
    for prvocislo in seznam_prvocisel:
        print(prvocislo)
