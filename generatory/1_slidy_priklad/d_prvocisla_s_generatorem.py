from a_problem import je_cislo_prvocislo


def generator_prvocisel(pozadovany_pocet_prvocisel):
    cislo = 2
    pocet_vygenerovanych_prvocisel = 0
    while pocet_vygenerovanych_prvocisel != pozadovany_pocet_prvocisel:
        if je_cislo_prvocislo(cislo):
            yield cislo
            pocet_vygenerovanych_prvocisel += 1
        cislo += 1


generator = generator_prvocisel(100_000)
for cislo in generator:
    print(cislo)


# Vytvoreni generatoru pomoci funkce a klicoveho slova yield je obecne mnohem jednodussi a citelnejsi, nez vytvoreni
# iteratoru. Proto pokud nepotrebujeme nejake slozitejsi zpracovani predmetu, ktere generujeme, je lepsi vytvorit
# generator.

# Dulezite je si uvedomit, ze iterator i generator jsou pouze jednocestne! Nejde je prochazet zpet. Pokud chci
# projit posloupnost znova, musim vytvorit novy generator

# Zaroven si lze predstavit, ze obecne iterator se pouziva na iterovani nad nejakou kolekci (seznam, ntice) a generator
# je velmi vhodny pro nekonecne posloupnosti.

# Obecne mi v mnoha pripadech staci pouze generator a iterator bych mel pouzit pokud mam nejakou komplexnejsi logiku
# pro udrzovani stavu, viz jako contextvoy manazer pomoci contextlib nebo tridy
