import sys

# stejne tak jako vytvarime list comprehensions, tak muzeme delat i generator comprehensions

seznam_cisel = [cislo for cislo in range(1_000_000)]
print(type(seznam_cisel))
# velikost objektu v nasi pameti RAM v bytech (B) - pouze orientacni
print(sys.getsizeof(seznam_cisel))

# zapise je uplne stejny, jenom staci nahradit kulate zavorky za hranate
seznam_cisel_generator = (cislo for cislo in range(1_000_000))
print(type(seznam_cisel_generator))
print(sys.getsizeof(seznam_cisel_generator))


# ukazat iter(list) a next(list)
# proc list neni iterator?
