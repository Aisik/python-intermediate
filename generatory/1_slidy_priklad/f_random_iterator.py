import random


class RandomOrderIterator:
    def __init__(self, data):
        """Initialize the iterator with the data list and shuffle it."""
        self.data = data.copy()  # Make a copy of the list to shuffle
        random.shuffle(self.data)  # Shuffle the list in-place
        self.index = 0  # Initialize the position index

    def __iter__(self):
        """Return the iterator object itself."""
        return self

    def __next__(self):
        """Return the next element from the shuffled list."""
        if self.index < len(self.data):
            result = self.data[self.index]
            self.index += 1
            return result
        else:
            raise StopIteration  # No more elements


# Example usage
my_list = [1, 2, 3, 4, 5]
random_iterator = RandomOrderIterator(my_list)

for element in random_iterator:
    print(element)
