class CiselnaSekvence:
    """ Nekonecna ciselna sekvence. """

    def __init__(self, start=0):
        self.aktualni_cislo = start

    def dalsi(self):
        aktualni_cislo = self.aktualni_cislo
        self.aktualni_cislo += 1
        return aktualni_cislo

# >>> sekvence = CiselnaSekvence()
# >>> sekvence.dalsi()
# 0
# >>> sekvence.dalsi()
# 1
# >>> sekvence.dalsi()
# 2
# >>> sekvence.dalsi()
# 3
# >>> for cislo in sekvence:
# ...     print(cislo)
# ...
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: 'CiselnaSekvence' object is not iterable
# >>> for index, cislo in enumerate(sekvence):
# ...     print(index)
# ...     print(cislo)
# ...
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: 'CiselnaSekvence' object is not iterable
# >>>


# musime to transformovat v iterator
class CiselnaSekvenceIterator:
    """ Nekonecna ciselna sekvence pomoci iteratoru. """
    def __init__(self, start=0):
        # stav, ktery nam umi iterator udrzovat a z ktereho lze generovat dalsi stavy - skrze next()
        self.aktualni_cislo = start

    def __next__(self):
        """ Specialni metoda, ktera dela objekt iteratorem (iterator) - muzeme volat 'next()' """
        aktualni_cislo = self.aktualni_cislo
        self.aktualni_cislo += 1
        return aktualni_cislo

    def __iter__(self):
        """ Specialni metoda, ktera dela objekt itterovatelnym (iterable) - muzeme volat 'for x in instance:' """
        return self


# >>> sekvence = CiselnaSekvenceIterator()
# >>> sekvence.__next__()
# 0
# >>> sekvence.__next__()
# 1
# >>> next(sekvence)
# 2
# >>> next(sekvence)
# 3
# >>> for cislo in sekvence:
# ...     print(cislo)
# KeyboardInterrupt
# co by se stalo, kdyby se to pustilo? Zacli bychom vypisovat nekonecnou sekvenci cisel


class CiselnaSekvenceIteratorLepe:
    """ Nekonecna ciselna sekvence pomoci iteratoru. """
    def __init__(self, start=0, maximalni_cislo=0):
        self.aktualni_cislo = start
        self.maximalni_cislo = maximalni_cislo

    def __next__(self):
        if self.aktualni_cislo >= self.maximalni_cislo:
            raise StopIteration
        aktualni_cislo = self.aktualni_cislo
        self.aktualni_cislo += 1
        return aktualni_cislo

    def __iter__(self):
        return self


# >>> sekvence = CiselnaSekvenceIteratorLepe()
# >>> next(sekvence)
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
#   File "b_uvod_iterator.py", line 75, in __next__
#     raise StopIteration
# StopIteration
# >>> for cislo in sekvence:
# ...     print(cislo)
# ...
# >>> sekvence = CiselnaSekvenceIteratorLepe(maximalni_cislo=10)
# >>> next(sekvence)
# 0
# >>> next(sekvence)
# 1
# >>> for cislo in sekvence:
# ...     print(cislo)
# ...
# 2
# 3
# 4
# 5
# 6
# 7
# 8
# 9

# MUZEME ITEROVAT POUZE SMEREM DOPREDU

# Cele to muze byt zjednoduseno vytvorenim jednoducheho generatoru:
def sekvence_pomoci_generatoru(start=0, maximalni_cislo=0):
    aktualni_cislo = start
    while True:
        # lze prvne zkusit bez podminky pouze s volanim 'next()'
        if aktualni_cislo >= maximalni_cislo:
            return
        yield aktualni_cislo
        aktualni_cislo += 1


# Klicove slovo yield dela z tela funkce generator. Protoze je to generator, je v podstate v pohode mit nekonecny
# while cyklus v tele generatoru, protoze to sice zatici procesor, ale pameti zabereme uplne minumum. Pokud bychom
# neco podobneho delali bez generatoru, ale pomoci normalni funkce, kde bychom pridavali prvku do listu, list by
# byl postupne tak velky, ze by nam to zaplnilo veskerou operacni pamet a pocitac by zamrzl.

# Generator obecne funguje tak, ze vykonava svoje telo, dokud nenarazi na yield, pomoci ktereho vrati dany prvek.
# Jakmile dojde generator na konec tela funkce (generatoru), generovani je ukonceno.
