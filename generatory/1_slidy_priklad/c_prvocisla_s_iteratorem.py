from a_problem import je_cislo_prvocislo
import sys


class IteratorPrvocisel:
    """
    Iterator, ktery umoznuje iterovat nad n prvocisly
    """
    def __init__(self, pozadovany_pocet_prvocisel):
        self.pozadovany_pocet_prvocisel = pozadovany_pocet_prvocisel
        self.pocet_vygenerovanych_prvocisel = 0
        self.aktualni_cislo = 1

    def __iter__(self):
        """ Specialni metoda, ktera nam umozni iterovat nad instanci tridy - umozni volat 'for x in instance' """
        return self

    def __next__(self):
        """ Specialni metoda, ktera  """
        self.aktualni_cislo += 1
        if self.pocet_vygenerovanych_prvocisel >= self.pozadovany_pocet_prvocisel:
            raise StopIteration
        elif je_cislo_prvocislo(self.aktualni_cislo):
            self.pocet_vygenerovanych_prvocisel += 1
            return self.aktualni_cislo
        return self.__next__()


iterator = IteratorPrvocisel(100_000)
print(sys.getsizeof(iterator))
for cislo in iterator:
    print(cislo)


# Po spusteni iteratoru plynou hned dve vyhody:
#   1. cisla generujeme postupne a kazde vygenerovane cislo ihned zpracujeme (ihned se objevi na vystupu a nemusime
#      cekat, az se vygeneruji vsechny)
#   2. nas program potrebuje mnohem mene pameti, protoze si v podstate udrzuje jen maly objekt s tremi instancnimi
#      promennymi
#           toto plati pozdeji i pro funkce jako map(), filter(), atd. Oni vraci taky iterator a sice pamet zabira input
#            hodnota jako list, ale vysledek je v podstate generator, protoze to generuji z te vstupni hodnoty lazily
#           Takze je to take memory efficient
