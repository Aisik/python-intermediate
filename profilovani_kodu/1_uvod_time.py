import time


# f(0) = 0
# f(1) = 1
# f(2) = f(1) + f(0) = 1 + 0 = 1
# f(3) = f(2) + f(1) = 1 + 1 = 2
# f(4) = f(3) + f(2) = 2 + 1 = 3
# f(5) = f(4) + f(3) = 3 + 2 = 5
# f(6) = f(5) + f(4) = 5 + 3 = 8
# f(7) = f(6) + f(5) = 8 + 5 = 13
# f(8) = f(7) + f(6) = 13 + 8 = 21
# f(9) = f(8) + f(7) = 21 + 13 = 34
# f(10) = f(9) + f(8) = 34 + 21 = 55
# ...

cache = {}


def fibbonaciho_sekvence(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibbonaciho_sekvence(n-1) + fibbonaciho_sekvence(n-2)


start = time.time()
soucet = fibbonaciho_sekvence(35)
print(soucet)
end = time.time() - start
print(end)


