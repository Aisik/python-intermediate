import timeit


# setup = "from math import sqrt"
code_sqrt = """
def func():
        return [sqrt(x) for x in range(100)]
"""


code = """
def fibbonaciho_sekvence(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibbonaciho_sekvence(n-1) + fibbonaciho_sekvence(n-2)
        
fibbonaciho_sekvence(25)
"""


POCET_OPAKOVANI = 100
print(timeit.timeit(code, number=POCET_OPAKOVANI)/POCET_OPAKOVANI)


