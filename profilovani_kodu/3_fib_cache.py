import time


cache = {}


def fibbonaciho_sekvence(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        n_1, n_2 = n - 1, n - 2
        vysledek = cache.get((n_1, n_2))
        if not vysledek:
            vysledek = fibbonaciho_sekvence(n-1) + fibbonaciho_sekvence(n-2)
            cache[(n_1, n_2)] = vysledek
        return vysledek


start = time.time()
soucet = fibbonaciho_sekvence(500)
print(soucet)
end = time.time() - start
print(end)
