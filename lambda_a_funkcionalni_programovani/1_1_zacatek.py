def pricti_jedna(x):
    return x + 1


vysledek = (lambda x: x + 1)(2)
print(vysledek)


zmensit = lambda x: x.lower()
print(zmensit("AHOJ"))



secist = lambda x, y: x + y
print(secist(3, 6))


je_cislo_kladne_a_nasobkem_desti = lambda x: x > 0 and x % 10 == 0
print(je_cislo_kladne_a_nasobkem_desti(-30))