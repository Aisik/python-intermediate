from a_prekladac_bez_lambdy import rozparsuj_math_soubor, zapis_zpracovany_math_soubor

# zde se nadherne hodi lambda, protoze nam usetri spoustu kodu, ale jen pokud nepotrebujeme neco navic v tele operace
# napr. logovani
slovnik_operatoru = {
    '+': lambda a, b: a + b,
    '-': lambda a, b: a - b
}


def zpracuj_math_radky(nactene_math_radky):
    zpracovane_radky = []
    for index, radek in enumerate(nactene_math_radky):
        a, operator, b = int(radek[0]), radek[1], int(radek[2])
        operace = slovnik_operatoru.get(operator)
        if operace:
            zpracovane_radky.append(operace(a, b))
        else:
            print(f"Operator '{operator}' na radku {index + 1} neni podporovan!")
    return zpracovane_radky


if __name__ == "__main__":
    nactene_radky = rozparsuj_math_soubor("nas_vlastni_program.math")
    zpracovane_radky = zpracuj_math_radky(nactene_radky)
    zapis_zpracovany_math_soubor(zpracovane_radky)
    print(nactene_radky)
    print(zpracovane_radky)
