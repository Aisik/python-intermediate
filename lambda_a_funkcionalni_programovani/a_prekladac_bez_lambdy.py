def scitani(a, b):
    return a + b


def odcitani(a, b):
    return a - b


def rozparsuj_math_soubor(cesta_k_souboru):
    assert cesta_k_souboru.endswith(".math"), "Predan spatny soubor, nazev musi koncit '.math'!"
    with open(cesta_k_souboru) as soubor_math:
        zdrojove_radky = []
        for radek in soubor_math:
            tokeny_radku = [token.strip() for token in radek.split(" ")]
            zdrojove_radky.append(tokeny_radku)
    return zdrojove_radky


def zpracuj_math_radky(nactene_math_radky):
    zpracovane_radky = []
    for index, radek in enumerate(nactene_math_radky):
        a, operator, b = int(radek[0]), radek[1], int(radek[2])
        if operator == '+':
            zpracovane_radky.append(scitani(a, b))
        elif operator == '-':
            zpracovane_radky.append(odcitani(a, b))
        else:
            print(f"Operator '{operator}' na radku {index + 1} neni podporovan!")
    return zpracovane_radky


def zapis_zpracovany_math_soubor(zpracovane_radky):
    with open("vysledek_naseho_programu.txt", 'w') as soubor_vysledek:
        soubor_vysledek.writelines([str(radek) + "\n" for radek in zpracovane_radky])


if __name__ == "__main__":
    nactene_radky = rozparsuj_math_soubor("nas_vlastni_program.math")
    zpracovane_radky = zpracuj_math_radky(nactene_radky)
    zapis_zpracovany_math_soubor(zpracovane_radky)
    print(nactene_radky)
    print(zpracovane_radky)
