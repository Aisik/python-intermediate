seznam_prvku = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
seznam_prvku_2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# funkce map
# https://docs.python.org/3/library/functions.html#map
mocniny_prvku = [x ** 2 for x in seznam_prvku]
# funkce map, filter i reduce maji navratovy typ generator
mocniny_prvku_map = list(map(lambda x: x ** 2, seznam_prvku))
print("----- Funkce map:")
print(mocniny_prvku)
print(mocniny_prvku_map)

sectena_cisla = [x + y for x, y in zip(seznam_prvku, seznam_prvku_2)]
sectena_cisla_map = list(map(lambda x, y: x + y, seznam_prvku, seznam_prvku_2))
print(sectena_cisla)
print(sectena_cisla_map)


# ----- funkce filter -----
# https://docs.python.org/3/library/functions.html#filter
cisla_vetsi_nez_tri = [cislo for cislo in seznam_prvku if cislo > 3]
cislo_vetsi_nez_tri_filter = list(filter(lambda x: x > 3, seznam_prvku))
print("----- Funkce filter:")
print(cisla_vetsi_nez_tri)
print(cislo_vetsi_nez_tri_filter)


def ciselny_filtr(x):
    return x > 0 and x % 2 == 0 and x % 3 == 0


vyfiltrovany_seznam = [cislo for cislo in seznam_prvku if ciselny_filtr(cislo)]
vyfiltrovany_seznam_filter = list(filter(ciselny_filtr, seznam_prvku))
vyfiltrovany_seznam_filter_lambda = list(filter(lambda x: x > 0 and x % 2 == 0 and x % 3 == 0, seznam_prvku))
print(vyfiltrovany_seznam)
print(vyfiltrovany_seznam_filter)
print(vyfiltrovany_seznam_filter_lambda)


# ----- funkce reduce -----
# https://docs.python.org/3/library/functools.html#functools.reduce
from functools import reduce

pronasobeny_list = reduce(lambda x, y: x * y, seznam_prvku)
print("----- Funkce reduce:")
print(pronasobeny_list)


# ----- lambda s parametrem key -----
print("----- Lambda s parametrem key:")
pary_cisel = [(1, 10), (2, 9), (3, 8)]
print(sorted(pary_cisel, key=lambda x: x[1]))
print(max(pary_cisel, key=lambda x: x[1]))
